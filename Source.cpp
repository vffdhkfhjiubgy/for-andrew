/*****************************************
 *                                       *
 *    Created by Patryk Gronkiewicz      *
 *             19.09.2015                *
 *                                       *
 *****************************************/


#include <iostream>

using namespace std;

int main() {
	cout << "Ile pieniedzy wydasz?" << endl;
	float koszt;            //definiowanie kosztu
	cin >> koszt;
	cout << "Ile pieniedzy wrzucasz?" << endl;
	float iloscPieniedzy;   //definiowanie ilo�ci wprowadzonych pieni�dzy
	cin >> iloscPieniedzy;  //wprowadzenie ilo�ci pieni�dzy

	float reszta = iloscPieniedzy - koszt;

	while (reszta != 0) {

		/* je�eli ilo�� pieni�dzy jest mniejsza lub r�wna 0 */
		if (reszta <= 0) {
			cout << "nie ma takiej mozliwosci..." << endl;      /*wykona si� to*/
		}
		else {

			//wydawanie banknot�w 200z�
			while (reszta >= 200) {                                /*lub to*/
				cout << "200zl" << endl;
				reszta = reszta - 200;
			}

			//wydawanie banknot�w 100z�
			while (reszta >= 100) {
				cout << "100zl" << endl;
				reszta = reszta - 100;
			}

			//wydawanie banknot�w 50z�
			while (reszta >= 50) {
				cout << "50zl" << endl;
				reszta = reszta - 50;
			}

			//wydawanie banknot�w 20z�
			while (reszta >= 20) {
				cout << "20zl" << endl;
				reszta = reszta - 20;
			}

			//wydawanie banknot�w 10z�
			while (reszta >= 10) {
				cout << "10zl" << endl;
				reszta = reszta - 10;
			}

			//wydawanie monet 5z�
			while (reszta >= 5) {
				cout << "5zl" << endl;
				reszta = reszta - 5;
			}

			//wydawanie monet 2z�
			while (reszta >= 2) {
				cout << "2zl" << endl;
				reszta = reszta - 2;
			}

			//wydawanie monet 1z�
			while (reszta >= 1) {
				cout << "1zl" << endl;
				reszta = reszta - 1;
			}

			//wydawanie monet 50gr
			while (reszta >= 0.5) {
				cout << "50gr" << endl;
				reszta = reszta - 0.5;
			}

			//wydawanie monet 20gr
			while (reszta >= 0.2) {
				cout << "20gr" << endl;
				reszta = reszta - 0.2;
			}

			//wydawanie monet 10gr
			while (reszta >= 0.1) {
				cout << "10gr" << endl;
				reszta = reszta - 0.1;
			}

			//wydawanie monet 5gr
			while (reszta >= 0.05) {
				cout << "5gr" << endl;
				reszta = reszta - 0.05;
			}

			//wydawanie monet 2gr
			while (reszta >= 0.02) {
				cout << "2gr" << endl;
				reszta = reszta - 0.02;
			}

			//wydawanie monet 1gr
			while (reszta >= 0.01) {
				cout << "1gr" << endl;
				reszta = reszta - 0.01;
			}


		}
		break;
	}

	/* Visual Studio zamyka�o program, wi�c to wstawi�em */
	cout << "wcisnij klawisz aby wyjsc" << endl;
	int koniec;
	cin >> koniec;
	
	return 0;			//funcja musi co� zwraca�
}
